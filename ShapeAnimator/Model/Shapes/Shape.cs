using System;
using System.Drawing;
using ShapeAnimator.View.Shapes;
using ShapeAnimator.View.Shapes.SpriteUtility;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Holds information about a shape
    /// </summary>
    public abstract class Shape
    {
        #region Instance variables

        private const int MinWidthAndHeight = 20;
        private const int MaxWidthAndHeight = 100;
        private Color color;
        private Point location;
        private ShapeSprite sprite;

        #endregion

        #region Properties

        /// <summary>
        ///     Sets the sprite.
        /// </summary>
        /// <value>
        ///     The sprite.
        /// </value>
        public ShapeSprite Sprite
        {
            get { return this.sprite; }
            set { this.sprite = value; }
        }

        /// <summary>
        ///     Gets or sets the x location of the shape.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.location.X; }
            set { this.location.X = value; }
        }

        /// <summary>
        ///     Gets the maximum x.
        /// </summary>
        /// <value>
        ///     The maximum x.
        /// </value>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location.Y = value; }
        }

        /// <summary>
        ///     Gets the width.
        /// </summary>
        /// <value>
        ///     The width.
        /// </value>
        public int Width { get; private set; }

        /// <summary>
        ///     Gets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public int Height { get; private set; }

        /// <summary>
        ///     Gets the area.
        /// </summary>
        /// <value>
        ///     The area.
        /// </value>
        public double Area
        {
            get { return this.CalculateArea(); }
        }

        /// <summary>
        ///     Gets the perimeter.
        /// </summary>
        /// <value>
        ///     The perimeter.
        /// </value>
        public double Perimeter
        {
            get { return this.CalculatePerimeter(); }
        }

        /// <summary>
        ///     Gets the bounded rectangle.
        /// </summary>
        /// <value>
        ///     The bounded rectangle.
        /// </value>
        public System.Drawing.Rectangle BoundedRectangle
        {
            get { return new System.Drawing.Rectangle(this.X, this.Y, this.Width, this.Height); }
        }

        /// <summary>
        ///     Gets or sets the color.
        /// </summary>
        /// <value>
        ///     The color.
        /// </value>
        public Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        /// <summary>
        ///     Gets the x speed.
        /// </summary>
        /// <value>
        ///     The x speed.
        /// </value>
        public int XDirection { get; set; }

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        /// <value>
        ///     The speed.
        /// </value>
        public int YDirection { get; set; }

        /// <summary>
        ///     Gets or sets the collisions.
        /// </summary>
        /// <value>
        ///     The collisions.
        /// </value>
        public int Collisions { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor
        /// </summary>
        private Shape()
        {
            this.setRandomDirection();
            this.X = 0;
            this.Y = 0;
            this.Color = ColorFactory.GetARandomColor();
            this.Width = Utility.RandomGeneratorNext(MinWidthAndHeight, MaxWidthAndHeight);
            this.Height = Utility.RandomGeneratorNext(MinWidthAndHeight, MaxWidthAndHeight);
            this.Collisions = 0;
        }

        /// <summary>
        ///     Constructs a shape at specified location
        ///     Precondition: location != null
        ///     Postcondition: X == location.X; Y == location.Y
        /// </summary>
        /// <param name="location">Location to create shape</param>
        /// <exception cref="System.ArgumentNullException">location</exception>
        protected Shape(Point location) : this()
        {
            if (location == null)
            {
                throw new ArgumentNullException("location");
            }

            this.location = location;
        }

        /// <summary>
        ///     Constructs a shape specified x,y location
        ///     Precondition: None
        ///     Postcondition: X == x; Y == y
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        protected Shape(int x, int y) : this()
        {
            this.location = new Point(x, y);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the area.
        /// </summary>
        /// <returns>The area of the shape</returns>
        protected abstract double CalculateArea();

        /// <summary>
        ///     Gets the perimeter.
        /// </summary>
        /// <returns>The perimeter of the shape</returns>
        protected abstract double CalculatePerimeter();

        /// <summary>
        ///     Randomly moves a shape in the x, y direction
        ///     Precondition: None
        ///     Postcondition: X == X@prev + amount of movement in X direction; Y == Y@prev + amount of movement in Y direction
        /// </summary>
        public void Move()
        {
            this.Y += this.YDirection;
            this.X += this.XDirection;
        }

        /// <summary>
        ///     Draws a shape
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        /// <exception cref="System.ArgumentNullException">g</exception>
        public void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            this.sprite.Paint(g);
        }

        /// <summary>
        ///     Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        ///     A string that represents the current object.
        /// </returns>
        public override String ToString()
        {
            String type = this.GetType().Name;
            string color = this.Color.R + " " + this.Color.G + " " + this.Color.B;
            String perimeter = this.Perimeter.ToString("0.000");
            String area = this.Area.ToString("0.000");
            String collisions = this.Collisions.ToString("0");

            string output = string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}", type, color, perimeter, area,
                collisions);

            return output;
        }

        #endregion

        #region Private Helpers

        private void setRandomDirection()
        {
            this.XDirection = 0;
            this.YDirection = 0;
            while (this.YDirection == 0 || this.XDirection == 0)
            {
                this.XDirection = Utility.RandomGeneratorNext(-5, 5);
                this.YDirection = Utility.RandomGeneratorNext(-5, 5);
            }
        }

        #endregion
    }
}