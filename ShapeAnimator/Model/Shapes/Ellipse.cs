﻿using System;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Models a circle shape to be moved. Derived from shape class
    /// </summary>
    public class Ellipse : Shape
    {
        #region Constructors

        /// <summary>
        ///     Default constructor for circle shape
        /// </summary>
        public Ellipse() : base(0, 0)
        {
            this.Sprite = new EllipseSprite(this);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Ellipse" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Ellipse(int x, int y) : base(x, y)
        {
            this.Sprite = new EllipseSprite(this);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates the circumference of the circle.
        /// </summary>
        /// <returns>
        ///     returns the calculated circumference of the circle.
        /// </returns>
        protected override double CalculatePerimeter()
        {
            double semiAxisX = (this.Width/2.0);
            double semiAxisY = (this.Height/2.0);

            double circumference = (2*Math.PI*(Math.Sqrt(((semiAxisX*semiAxisX) + (semiAxisY*semiAxisY))/2)));

            return circumference;
        }

        /// <summary>
        ///     Caculates the area of the circle
        /// </summary>
        /// <returns>
        ///     returns the calculated area of the circle
        /// </returns>
        protected override double CalculateArea()
        {
            double semiAxisX = (this.Width/2.0);
            double semiAxisY = (this.Height/2.0);

            double area = (Math.PI*semiAxisX*semiAxisY);

            return area;
        }

        #endregion
    }
}