﻿using System;
using ShapeAnimator.View.Shapes.Decorators;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     A shape factory class
    /// </summary>
    public static class ShapeFactory
    {
        private static Shape theShape;

        #region Data Members

        /// <summary>
        ///     Public enum types to be used to create a shape
        /// </summary>
        public enum Shapes
        {
            /// <summary>
            ///     The circle
            /// </summary>
            Circle,

            /// <summary>
            ///     The rectangle
            /// </summary>
            Rectangle,

            /// <summary>
            ///     The random shape
            /// </summary>
            RandomShape
        }

        private enum ShapePatterns
        {
            NoPattern,
            HorizontalLine,
            VerticalLine,
            Spotted
        }

        #endregion

        #region Methods        

        /// <summary>
        ///     Creates a shape.
        /// </summary>
        /// <param name="shapeType">Type of the shape.</param>
        /// <returns></returns>
        public static Shape CreateAShape(Shapes shapeType)
        {
            theShape = null;
            switch (shapeType)
            {
                case Shapes.Circle:
                    theShape = new Ellipse();

                    break;
                case Shapes.Rectangle:
                    theShape = new Rectangle();

                    break;
                case Shapes.RandomShape:
                    theShape = getARandomShape();

                    break;
            }

            addMultiplePatternsToShape();
            return theShape;
        }

        /// <summary>
        ///     Gets a random shape.
        /// </summary>
        /// <returns></returns>
        private static Shape getARandomShape()
        {
            theShape = null;
            Array values = Enum.GetValues(typeof (Shapes));

            var randomShape = (Shapes) values.GetValue(Utility.RandomGeneratorNext(values.Length));

            switch (randomShape)
            {
                case Shapes.Circle:
                    theShape = new Ellipse();

                    break;
                case Shapes.Rectangle:
                    theShape = new Rectangle();

                    break;
                case Shapes.RandomShape:
                    theShape = getARandomShape();

                    break;
            }

            return theShape;
        }

        /// <summary>
        ///     Adds three random decorator patterns to the shape.
        /// </summary>
        private static void addMultiplePatternsToShape()
        {
            ShapePatterns firstPattern = getARandomShapePatternType();
            ShapePatterns secondPattern = getARandomShapePatternType();
            ShapePatterns thirdPattern = getARandomShapePatternType();

            while (firstPattern != ShapePatterns.NoPattern && firstPattern == secondPattern)
            {
                secondPattern = getARandomShapePatternType();
            }

            while (secondPattern != ShapePatterns.NoPattern && secondPattern == thirdPattern)
            {
                thirdPattern = getARandomShapePatternType();
            }

            addPatternsToShape(firstPattern);
            addPatternsToShape(secondPattern);
            addPatternsToShape(thirdPattern);
        }

        /// <summary>
        ///     Gets the type of a random shape pattern Enum type;
        /// </summary>
        /// <returns></returns>
        private static ShapePatterns getARandomShapePatternType()
        {
            Array values = Enum.GetValues(typeof (ShapePatterns));
            var randomShape = (ShapePatterns) values.GetValue(Utility.RandomGeneratorNext(values.Length));
            return randomShape;
        }

        /// <summary>
        ///     Adds a shape pattern to shape
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        private static void addPatternsToShape(ShapePatterns pattern)
        {
            if (pattern == ShapePatterns.HorizontalLine)
            {
                theShape.Sprite = new HorizontalLine(theShape.Sprite);
            }
            else if (pattern == ShapePatterns.VerticalLine)
            {
                theShape.Sprite = new VerticalLine(theShape.Sprite);
            }
            else if (pattern == ShapePatterns.Spotted)
            {
                theShape.Sprite = new Spot(theShape.Sprite);
            }
            else
            {
                theShape.Sprite = theShape.Sprite;
            }
        }

        #endregion
    }
}