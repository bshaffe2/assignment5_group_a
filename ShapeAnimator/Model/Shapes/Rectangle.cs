﻿using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Models a Rectangle to be moved. Derived from Shape class
    /// </summary>
    public class Rectangle : Shape
    {
        #region Constructors

        /// <summary>
        ///     Default constructor for Rectangle shape
        /// </summary>
        public Rectangle() : base(0, 0)
        {
            this.Sprite = new RectangleSprite(this);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Rectangle" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Rectangle(int x, int y) : base(x, y)
        {
            this.Sprite = new RectangleSprite(this);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates the perimeter of the rectangle
        /// </summary>
        /// <returns>The calculated perimeter</returns>
        protected override double CalculatePerimeter()
        {
            return (this.Height*2) + (this.Width*2);
        }

        /// <summary>
        ///     Calculates the area of the rectangle
        /// </summary>
        /// <returns>The calculated area</returns>
        protected override double CalculateArea()
        {
            return (this.Height*this.Width);
        }

        #endregion
    }
}