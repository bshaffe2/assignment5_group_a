﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Properties;

namespace ShapeAnimator.Model
{
    /// <summary>
    ///     Manages the collection of shapes on the canvas.
    /// </summary>
    public class ShapeManager
    {
        #region Instance variables

        private readonly PictureBox canvas;
        private readonly List<Shape> shapes;
        private Shape selectedShape;

        #endregion

        #region Properties        

        /// <summary>
        ///     Gets the shapes.
        /// </summary>
        /// <value>
        ///     The shapes.
        /// </value>
        public IEnumerable<Shape> Shapes
        {
            get { return this.shapes; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeManager" /> class from being created.
        /// </summary>
        private ShapeManager()
        {
            this.shapes = new List<Shape>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeManager" /> class.
        ///     Precondition: pictureBox != null
        /// </summary>
        /// <param name="pictureBox">The picture box that will be drawing on</param>
        /// <exception cref="System.ArgumentNullException">pictureBox</exception>
        public ShapeManager(PictureBox pictureBox) : this()
        {
            if (pictureBox == null)
            {
                throw new ArgumentNullException("pictureBox", Resources.PictureBoxNullMessage);
            }

            this.canvas = pictureBox;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Places the specified shape on the canvas.
        /// </summary>
        /// <param name="numberOfRandomShapes">The number of random shapes.</param>
        /// <param name="numberOfCircleShapes">The number of circle shapes.</param>
        /// <param name="numberOfRectangleShapes">The number of rectangle shapes.</param>
        public void PlaceSpecifiedShapesOnCanvas(int numberOfRandomShapes, int numberOfCircleShapes,
            int numberOfRectangleShapes)
        {
            this.addShape(numberOfRandomShapes, ShapeFactory.Shapes.RandomShape);
            this.addShape(numberOfCircleShapes, ShapeFactory.Shapes.Circle);
            this.addShape(numberOfRectangleShapes, ShapeFactory.Shapes.Rectangle);
        }

        /// <summary>
        ///     Moves the shape around and the calls the Shape::Paint method to draw the shape.
        ///     Precondition: g != null
        /// </summary>
        /// <param name="g">The graphics object to draw on</param>
        /// <param name="isPaused"></param>
        /// <exception cref="System.ArgumentNullException">g</exception>
        public void Update(Graphics g, bool isPaused)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if ((this.shapes != null) && !isPaused)
            {
                this.moveShapes();
                this.paintShapes(g);
            }
            else if (this.shapes != null && isPaused)
            {
                this.paintShapes(g);
            }
        }

        /// <summary>
        ///     Clears the shapes from canvas.
        /// </summary>
        public void ClearShapes()
        {
            this.shapes.Clear();
        }

        /// <summary>
        ///     Gets the area sorted collection of shapes.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Shape> GetAreaSortedCollection()
        {
            IOrderedEnumerable<Shape> query = from aShape in this.shapes
                orderby aShape.Area ascending
                select aShape;

            return query;
        }

        /// <summary>
        ///     Gets the perimeter sorted collection of shapes.
        /// </summary>
        /// <returns>Shapes sorted by Perimeter</returns>
        public IEnumerable<Shape> GetPerimeterSortedCollection()
        {
            IOrderedEnumerable<Shape> query = from aShape in this.shapes
                orderby aShape.Perimeter ascending
                select aShape;

            return query;
        }

        /// <summary>
        ///     Gets the 2 level sort type color sorted collection.
        /// </summary>
        /// <returns>Shapes sorted by Type then Color</returns>
        public IEnumerable<Shape> GetTypeColorSortedCollection()
        {
            IOrderedEnumerable<Shape> query = from aShape in this.shapes
                orderby aShape.GetType().Name, aShape.Color.ToArgb() ascending
                select aShape;

            return query;
        }

        /// <summary>
        ///     Gets the 2 level collision type sorted collection.
        /// </summary>
        /// <returns>Shapes sorted by Collision count then Type</returns>
        public IEnumerable<Shape> GetCollisionTypeSortedCollection()
        {
            IOrderedEnumerable<Shape> query = from aShape in this.shapes
                orderby aShape.Collisions, aShape.GetType().Name ascending
                select aShape;

            return query;
        }

        /// <summary>
        ///     Locations the contains shape.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public bool LocationContainsShape(int x, int y)
        {
            bool locationHasShape = false;

            foreach (Shape aShape in this.shapes)
            {
                bool isInShapeYRange = aShape.BoundedRectangle.Contains(x,y);
                bool isInShapeXRange = aShape.BoundedRectangle.Contains(x, y);

                if (isInShapeXRange && isInShapeYRange)
                {
                    locationHasShape = true;
                    this.selectedShape = aShape;
                }
            }

            return locationHasShape;
        }

        /// <summary>
        ///     Changes the color of the shape.
        /// </summary>
        public void ChangeShapeColor()
        {
            var colorChanger = new ColorDialog
            {
                CustomColors = new[]
                {
                    ColorTranslator.ToWin32(this.selectedShape.Color)
                }
            };
            if (colorChanger.ShowDialog() == DialogResult.OK)
            {
                this.selectedShape.Color = colorChanger.Color;
            }
        }

        #endregion

        #region Private Helpers

        private void addShape(int numberOfShapes, ShapeFactory.Shapes shapeType)
        {
            for (int i = 0; i < numberOfShapes; i++)
            {
                Shape aShape = ShapeFactory.CreateAShape(shapeType);
                this.updateShapeLocation(aShape);
                this.shapes.Add(aShape);
            }
        }

        private void updateShapeLocation(Shape aShape)
        {
            int x = Utility.RandomGeneratorNext(this.canvas.Width - aShape.Width);
            int y = Utility.RandomGeneratorNext(this.canvas.Height - aShape.Height);
            aShape.X = x;
            aShape.Y = y;
            this.placeShapesInDifferentLocations(aShape);
        }

        private void placeShapesInDifferentLocations(Shape aShape)
        {
            foreach (Shape anotherShape in this.shapes)
            {
                while (this.shapesOverlap(aShape, anotherShape))
                {
                    this.updateShapeLocation(aShape);
                }
            }
        }

        private bool shapesOverlap(Shape aShape, Shape anotherShape)
        {
            return (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
        }

        /// <summary>
        ///     moves all shapes in this.listOfShapes
        /// </summary>
        private void moveShapes()
        {
            foreach (Shape aShape in this.Shapes)
            {
                this.shapeCollisionCheck(aShape);
            }

            foreach (Shape aShape in this.shapes)
            {
                this.wallCollisionCheck(aShape);
                aShape.Move();
            }
        }


        /// <summary>
        /// checks if a shape overlaps with another shape in the list. If it does then both shapes change direction
        /// </summary>
        /// <param name="aShape">a shape.</param>
        private void shapeCollisionCheck(Shape aShape)
        {
            
            foreach (Shape anotherShape in this.Shapes)
            {
                const int reflection = -1;
                if (this.shapesOverlap(aShape, anotherShape) && !this.isShapesEqual(aShape, anotherShape))
                {
                    aShape.XDirection = aShape.XDirection*reflection;
                    aShape.YDirection = aShape.YDirection*reflection;
                    anotherShape.XDirection = anotherShape.XDirection*reflection;
                    anotherShape.YDirection = anotherShape.YDirection*reflection;
                    aShape.Collisions ++;
                    anotherShape.Collisions ++;
                    aShape.Move();
                    anotherShape.Move();
                }
            }
        }

        private bool isShapesEqual(Shape ashape, Shape anotherShape)
        {
            return (ashape == anotherShape);
        }

        /// <summary>
        ///     paints all shapes in this.listOfshapes
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        private void paintShapes(Graphics graphics)
        {
            foreach (Shape aShape in this.shapes)
            {
                aShape.Paint(graphics);
            }
        }

        /// <summary>
        ///     Checks if the shape has hit a wall. If shape hits a wall the direction is reflected
        ///     precondition: aShape != null
        ///     postcondition: if shape hits wall Direction = @prevDirection*-1
        /// </summary>
        /// <param name="aShape"> Shape ashape is the the shape being checked</param>
        private void wallCollisionCheck(Shape aShape)
        {
            const int reflection = -1;
            if (aShape == null)
            {
                throw new ArgumentException("aShape cannot be null");
            }

            bool verticalCollision = aShape.Y <= 0 || aShape.Y >= this.canvas.Height - aShape.Height;
            bool horizontalCollision = aShape.X <= 0 || aShape.X >= this.canvas.Width - aShape.Width;
            bool dualCollision = verticalCollision && horizontalCollision;

            if (dualCollision)
            {
                aShape.YDirection = aShape.YDirection*reflection;
                aShape.XDirection = aShape.XDirection*reflection;
                aShape.Collisions++;
            }

            else if (verticalCollision)
            {
                aShape.YDirection = aShape.YDirection*reflection;
                aShape.Collisions++;
            }
            else if (horizontalCollision)
            {
                aShape.XDirection = aShape.XDirection*reflection;
                aShape.Collisions++;
            }
        }

        #endregion
    }
}