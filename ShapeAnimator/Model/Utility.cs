﻿using System;

namespace ShapeAnimator.Model
{
    /// <summary>
    ///     Static Utility Class
    /// </summary>
    public static class Utility
    {
        #region Data Members

        /// <summary>
        ///     Static method that creates a randomGenerator
        /// </summary>
        private static readonly Random MyRandomGenerator = new Random();

        #endregion

        #region Constructors

        /// <summary>
        ///     Static constructor to make sure that the class cannot be Initialized
        /// </summary>
        static Utility()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Randoms the generator next.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        public static int RandomGeneratorNext(int val)
        {
            return MyRandomGenerator.Next(val);
        }

        /// <summary>
        ///     Randoms the generator next.
        /// </summary>
        /// <param name="val1">The val1.</param>
        /// <param name="val2">The val2.</param>
        /// <returns></returns>
        public static int RandomGeneratorNext(int val1, int val2)
        {
            return MyRandomGenerator.Next(val1, val2);
        }

        #endregion
    }
}