using System.Windows.Forms;
using ShapeAnimator.View.Forms;

namespace ShapeAnimator.View.Forms
{
    partial class ShapeAnimatorForm : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.numberOfShapesLabel = new System.Windows.Forms.Label();
            this.numberShapesTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.numberOfCircleTextBox = new System.Windows.Forms.TextBox();
            this.CircleLabel = new System.Windows.Forms.Label();
            this.RectangleLabel = new System.Windows.Forms.Label();
            this.numberOfRectangleTextBox = new System.Windows.Forms.TextBox();
            this.PauseButton = new System.Windows.Forms.Button();
            this.ResumeButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.Slider = new System.Windows.Forms.TrackBar();
            this.zeroLabel = new System.Windows.Forms.Label();
            this.hundredLabel = new System.Windows.Forms.Label();
            this.twoHundredLabel = new System.Windows.Forms.Label();
            this.threeHundredLabel = new System.Windows.Forms.Label();
            this.fourHundredLabel = new System.Windows.Forms.Label();
            this.fiveHundredLabel = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.sortLabel = new System.Windows.Forms.Label();
            this.AreaRadioButton = new System.Windows.Forms.RadioButton();
            this.PerimeterRadioButton = new System.Windows.Forms.RadioButton();
            this.TypeColorRadioButton = new System.Windows.Forms.RadioButton();
            this.CollisionTypeRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Slider)).BeginInit();
            this.SuspendLayout();
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.BackColor = System.Drawing.Color.Black;
            this.canvasPictureBox.Location = new System.Drawing.Point(14, 83);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(658, 415);
            this.canvasPictureBox.TabIndex = 0;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.Click += new System.EventHandler(this.canvasPictureBox_Click);
            this.canvasPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.shapeCanvasPictureBox_Paint);
            // 
            // animationTimer
            // 
            this.animationTimer.Interval = 200;
            this.animationTimer.Tick += new System.EventHandler(this.animationTimer_Tick);
            // 
            // numberOfShapesLabel
            // 
            this.numberOfShapesLabel.AutoSize = true;
            this.numberOfShapesLabel.Location = new System.Drawing.Point(11, 19);
            this.numberOfShapesLabel.Name = "numberOfShapesLabel";
            this.numberOfShapesLabel.Size = new System.Drawing.Size(72, 13);
            this.numberOfShapesLabel.TabIndex = 1;
            this.numberOfShapesLabel.Text = "# of Random:";
            // 
            // numberShapesTextBox
            // 
            this.numberShapesTextBox.Location = new System.Drawing.Point(89, 16);
            this.numberShapesTextBox.Name = "numberShapesTextBox";
            this.numberShapesTextBox.Size = new System.Drawing.Size(47, 20);
            this.numberShapesTextBox.TabIndex = 2;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(102, 54);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.animateButton_Click);
            // 
            // numberOfCircleTextBox
            // 
            this.numberOfCircleTextBox.Location = new System.Drawing.Point(225, 16);
            this.numberOfCircleTextBox.Name = "numberOfCircleTextBox";
            this.numberOfCircleTextBox.Size = new System.Drawing.Size(59, 20);
            this.numberOfCircleTextBox.TabIndex = 4;
            // 
            // CircleLabel
            // 
            this.CircleLabel.AutoSize = true;
            this.CircleLabel.Location = new System.Drawing.Point(153, 19);
            this.CircleLabel.Name = "CircleLabel";
            this.CircleLabel.Size = new System.Drawing.Size(65, 13);
            this.CircleLabel.TabIndex = 5;
            this.CircleLabel.Text = "# of Ellipse: ";
            // 
            // RectangleLabel
            // 
            this.RectangleLabel.AutoSize = true;
            this.RectangleLabel.Location = new System.Drawing.Point(300, 19);
            this.RectangleLabel.Name = "RectangleLabel";
            this.RectangleLabel.Size = new System.Drawing.Size(89, 13);
            this.RectangleLabel.TabIndex = 6;
            this.RectangleLabel.Text = "# of Rectangles: ";
            // 
            // numberOfRectangleTextBox
            // 
            this.numberOfRectangleTextBox.Location = new System.Drawing.Point(395, 16);
            this.numberOfRectangleTextBox.Name = "numberOfRectangleTextBox";
            this.numberOfRectangleTextBox.Size = new System.Drawing.Size(59, 20);
            this.numberOfRectangleTextBox.TabIndex = 7;
            // 
            // PauseButton
            // 
            this.PauseButton.Location = new System.Drawing.Point(242, 54);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Size = new System.Drawing.Size(75, 23);
            this.PauseButton.TabIndex = 10;
            this.PauseButton.Text = "Pause";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // ResumeButton
            // 
            this.ResumeButton.Location = new System.Drawing.Point(382, 54);
            this.ResumeButton.Name = "ResumeButton";
            this.ResumeButton.Size = new System.Drawing.Size(75, 23);
            this.ResumeButton.TabIndex = 11;
            this.ResumeButton.Text = "Resume";
            this.ResumeButton.UseVisualStyleBackColor = true;
            this.ResumeButton.Click += new System.EventHandler(this.ResumButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(522, 54);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 12;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // Slider
            // 
            this.Slider.Location = new System.Drawing.Point(4, 504);
            this.Slider.Maximum = 500;
            this.Slider.Name = "Slider";
            this.Slider.Size = new System.Drawing.Size(668, 45);
            this.Slider.TabIndex = 13;
            this.Slider.TickFrequency = 100;
            this.Slider.Value = 250;
            this.Slider.Scroll += new System.EventHandler(this.Slider_Scroll);
            // 
            // zeroLabel
            // 
            this.zeroLabel.AutoSize = true;
            this.zeroLabel.Location = new System.Drawing.Point(11, 536);
            this.zeroLabel.Name = "zeroLabel";
            this.zeroLabel.Size = new System.Drawing.Size(29, 13);
            this.zeroLabel.TabIndex = 14;
            this.zeroLabel.Text = "0 ms";
            // 
            // hundredLabel
            // 
            this.hundredLabel.AutoSize = true;
            this.hundredLabel.Location = new System.Drawing.Point(127, 536);
            this.hundredLabel.Name = "hundredLabel";
            this.hundredLabel.Size = new System.Drawing.Size(41, 13);
            this.hundredLabel.TabIndex = 15;
            this.hundredLabel.Text = "100 ms";
            // 
            // twoHundredLabel
            // 
            this.twoHundredLabel.AutoSize = true;
            this.twoHundredLabel.Location = new System.Drawing.Point(259, 536);
            this.twoHundredLabel.Name = "twoHundredLabel";
            this.twoHundredLabel.Size = new System.Drawing.Size(41, 13);
            this.twoHundredLabel.TabIndex = 16;
            this.twoHundredLabel.Text = "200 ms";
            // 
            // threeHundredLabel
            // 
            this.threeHundredLabel.AutoSize = true;
            this.threeHundredLabel.Location = new System.Drawing.Point(392, 536);
            this.threeHundredLabel.Name = "threeHundredLabel";
            this.threeHundredLabel.Size = new System.Drawing.Size(41, 13);
            this.threeHundredLabel.TabIndex = 17;
            this.threeHundredLabel.Text = "300 ms";
            // 
            // fourHundredLabel
            // 
            this.fourHundredLabel.AutoSize = true;
            this.fourHundredLabel.Location = new System.Drawing.Point(515, 536);
            this.fourHundredLabel.Name = "fourHundredLabel";
            this.fourHundredLabel.Size = new System.Drawing.Size(41, 13);
            this.fourHundredLabel.TabIndex = 18;
            this.fourHundredLabel.Text = "400 ms";
            // 
            // fiveHundredLabel
            // 
            this.fiveHundredLabel.AutoSize = true;
            this.fiveHundredLabel.Location = new System.Drawing.Point(632, 536);
            this.fiveHundredLabel.Name = "fiveHundredLabel";
            this.fiveHundredLabel.Size = new System.Drawing.Size(41, 13);
            this.fiveHundredLabel.TabIndex = 19;
            this.fiveHundredLabel.Text = "500 ms";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 594);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(661, 121);
            this.listBox1.TabIndex = 22;
            // 
            // sortLabel
            // 
            this.sortLabel.AutoSize = true;
            this.sortLabel.Location = new System.Drawing.Point(12, 569);
            this.sortLabel.Name = "sortLabel";
            this.sortLabel.Size = new System.Drawing.Size(44, 13);
            this.sortLabel.TabIndex = 27;
            this.sortLabel.Text = "Sort By:";
            // 
            // AreaRadioButton
            // 
            this.AreaRadioButton.AutoSize = true;
            this.AreaRadioButton.Location = new System.Drawing.Point(62, 567);
            this.AreaRadioButton.Name = "AreaRadioButton";
            this.AreaRadioButton.Size = new System.Drawing.Size(47, 17);
            this.AreaRadioButton.TabIndex = 28;
            this.AreaRadioButton.TabStop = true;
            this.AreaRadioButton.Text = "Area";
            this.AreaRadioButton.UseVisualStyleBackColor = true;
            this.AreaRadioButton.CheckedChanged += new System.EventHandler(this.AreaRadioButton_CheckedChanged);
            // 
            // PerimeterRadioButton
            // 
            this.PerimeterRadioButton.AutoSize = true;
            this.PerimeterRadioButton.Location = new System.Drawing.Point(130, 567);
            this.PerimeterRadioButton.Name = "PerimeterRadioButton";
            this.PerimeterRadioButton.Size = new System.Drawing.Size(69, 17);
            this.PerimeterRadioButton.TabIndex = 29;
            this.PerimeterRadioButton.TabStop = true;
            this.PerimeterRadioButton.Text = "Perimeter";
            this.PerimeterRadioButton.UseVisualStyleBackColor = true;
            this.PerimeterRadioButton.CheckedChanged += new System.EventHandler(this.PerimeterRadioButton_CheckedChanged);
            // 
            // TypeColorRadioButton
            // 
            this.TypeColorRadioButton.AutoSize = true;
            this.TypeColorRadioButton.Location = new System.Drawing.Point(225, 567);
            this.TypeColorRadioButton.Name = "TypeColorRadioButton";
            this.TypeColorRadioButton.Size = new System.Drawing.Size(78, 17);
            this.TypeColorRadioButton.TabIndex = 30;
            this.TypeColorRadioButton.TabStop = true;
            this.TypeColorRadioButton.Text = "Type/Color";
            this.TypeColorRadioButton.UseVisualStyleBackColor = true;
            this.TypeColorRadioButton.CheckedChanged += new System.EventHandler(this.TypeColorRadioButton_CheckedChanged);
            // 
            // CollisionTypeRadioButton
            // 
            this.CollisionTypeRadioButton.AutoSize = true;
            this.CollisionTypeRadioButton.Location = new System.Drawing.Point(335, 567);
            this.CollisionTypeRadioButton.Name = "CollisionTypeRadioButton";
            this.CollisionTypeRadioButton.Size = new System.Drawing.Size(92, 17);
            this.CollisionTypeRadioButton.TabIndex = 31;
            this.CollisionTypeRadioButton.TabStop = true;
            this.CollisionTypeRadioButton.Text = "Collision/Type";
            this.CollisionTypeRadioButton.UseVisualStyleBackColor = true;
            this.CollisionTypeRadioButton.CheckedChanged += new System.EventHandler(this.CollisionTypeRadioButton_CheckedChanged);
            // 
            // ShapeAnimatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(684, 727);
            this.Controls.Add(this.CollisionTypeRadioButton);
            this.Controls.Add(this.TypeColorRadioButton);
            this.Controls.Add(this.PerimeterRadioButton);
            this.Controls.Add(this.AreaRadioButton);
            this.Controls.Add(this.sortLabel);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.fiveHundredLabel);
            this.Controls.Add(this.fourHundredLabel);
            this.Controls.Add(this.threeHundredLabel);
            this.Controls.Add(this.twoHundredLabel);
            this.Controls.Add(this.hundredLabel);
            this.Controls.Add(this.zeroLabel);
            this.Controls.Add(this.Slider);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.ResumeButton);
            this.Controls.Add(this.PauseButton);
            this.Controls.Add(this.numberOfRectangleTextBox);
            this.Controls.Add(this.RectangleLabel);
            this.Controls.Add(this.CircleLabel);
            this.Controls.Add(this.numberOfCircleTextBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.numberShapesTextBox);
            this.Controls.Add(this.numberOfShapesLabel);
            this.Controls.Add(this.canvasPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ShapeAnimatorForm";
            this.Text = "Shape Animator A5 Yang Shaffer";
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Slider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.Timer animationTimer;
        private System.Windows.Forms.Label numberOfShapesLabel;
        private System.Windows.Forms.TextBox numberShapesTextBox;
        private System.Windows.Forms.Button startButton;
        private TextBox numberOfCircleTextBox;
        private Label CircleLabel;
        private Label RectangleLabel;
        private TextBox numberOfRectangleTextBox;
        private Button PauseButton;
        private Button ResumeButton;
        private Button ClearButton;
        private TrackBar Slider;
        private Label zeroLabel;
        private Label hundredLabel;
        private Label twoHundredLabel;
        private Label threeHundredLabel;
        private Label fourHundredLabel;
        private Label fiveHundredLabel;
        private ListBox listBox1;
        private Label sortLabel;
        private RadioButton AreaRadioButton;
        private RadioButton PerimeterRadioButton;
        private RadioButton TypeColorRadioButton;
        private RadioButton CollisionTypeRadioButton;
    }
}

