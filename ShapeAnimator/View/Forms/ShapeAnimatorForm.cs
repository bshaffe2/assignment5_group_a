using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ShapeAnimator.Model;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Forms
{
    /// <summary>
    ///     Manages the form that will display shapes.
    /// </summary>
    public partial class ShapeAnimatorForm
    {
        #region Instance variables

        private readonly ShapeManager canvasManager;

        #endregion

        #region Properties

        /// <summary>
        ///     Converts the text in the numberShapesTextBox to an integer. If the text
        ///     is not convertable to an integer value it returns 0.
        /// </summary>
        /// <value>
        ///     The number shapes.
        /// </value>
        public int NumberOfRandomShapes
        {
            get { return this.convertTextToNumber(this.numberShapesTextBox.Text); }
        }

        /// <summary>
        ///     Converts the text in the numberOfCircleshapes to an integer. If the text
        ///     is not convertable to an integer value it returns 0.
        /// </summary>
        /// <value>
        ///     The circle number shapes.
        /// </value>
        public int NumberOfCircleShapes
        {
            get { return this.convertTextToNumber(this.numberOfCircleTextBox.Text); }
        }

        /// <summary>
        ///     Converts the text in the numberOfRectangleShapes to an integer. If the text
        ///     is not convertable to an integer value it returns 0.
        /// </summary>
        /// <value>
        ///     The rectangle number shapes.
        /// </value>
        public int NumberOfRectangleShapes
        {
            get { return this.convertTextToNumber(this.numberOfRectangleTextBox.Text); }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeAnimatorForm" /> class.
        ///     Precondition: None
        /// </summary>
        public ShapeAnimatorForm()
        {
            this.InitializeComponent();
            this.canvasManager = new ShapeManager(this.canvasPictureBox);
            this.PauseButton.Enabled = false;
            this.ResumeButton.Enabled = false;
            this.ClearButton.Enabled = false;
            this.listBox1.Font = new Font(FontFamily.GenericMonospace, this.listBox1.Font.Size);
            this.addHeader();
        }

        #endregion

        #region Event generated methods

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void shapeCanvasPictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (this.animationTimer.Enabled)
            {
                this.canvasManager.Update(g, false);
            }
            else
            {
                this.canvasManager.Update(g, true);
            }

            this.refreshCanvasAndList(sender, e);
        }

        private void animateButton_Click(object sender, EventArgs e)
        {
            this.animationTimer.Stop();
            this.canvasManager.ClearShapes();
            this.animationTimer.Interval = (this.Slider.Maximum + 1);
            if (this.isValidShapeNumber())
            {
                this.canvasManager.PlaceSpecifiedShapesOnCanvas(this.NumberOfRandomShapes, this.NumberOfCircleShapes,
                    this.NumberOfRectangleShapes);
                this.enabledButtonsOnAnimate();
                this.disabledButtonsOnAnimate();
                this.addItemsToListBox(this.canvasManager.Shapes);
            }
            else
            {
                const string error = "Please enter a valid number. Value Cannot be greater than 10.";
                MessageBox.Show(error);
                this.numberShapesTextBox.ResetText();
            }

            this.animationTimer.Start();
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            this.animationTimer.Stop();
            this.PauseButton.Enabled = false;
            this.ResumeButton.Enabled = true;
        }

        private void ResumButton_Click(object sender, EventArgs e)
        {
            this.PauseButton.Enabled = true;
            this.ResumeButton.Enabled = false;
            this.listBox1.ResetText();
            this.animationTimer.Start();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.animationTimer.Stop();
            this.canvasManager.ClearShapes();
            this.animationTimer.Start();
            this.disableAllButtons();
            this.startButton.Enabled = true;
            this.resetTextBoxes();
            this.enableAllTextFields();
            this.Slider.Value = 250;
            this.listBox1.Items.Clear();
            this.addHeader();
        }

        private void Slider_Scroll(object sender, EventArgs e)
        {
            int initialIntervalSpeed = this.Slider.Maximum + 1;
            if (this.Slider.Value == 0)
            {
                this.animationTimer.Stop();
            }
            else
            {
                this.animationTimer.Start();
                this.animationTimer.Interval = initialIntervalSpeed - this.Slider.Value;
            }
        }

        #endregion

        #region Private Helper Methods

        private int convertTextToNumber(String textToBeConverted)
        {
            try
            {
                return Convert.ToInt32(textToBeConverted);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private Boolean isValidShapeNumber()
        {
            int sum = this.NumberOfRandomShapes + this.NumberOfCircleShapes + this.NumberOfRectangleShapes;

            return (sum >= 1 && sum <= 10 && this.NumberOfRandomShapes >= 0 && this.NumberOfCircleShapes >= 0 &&
                    this.NumberOfRectangleShapes >= 0);
        }

        private void addItemsToListBox(IEnumerable<Shape> collection)
        {
            if (collection == null)
            {
                throw new Exception("collection cannot be null");
            }

            this.listBox1.Items.Clear();
            this.addHeader();
            foreach (Shape aShape in collection)
            {
                this.listBox1.Items.Add(aShape.ToString());
            }
        }

        private void addHeader()
        {
            const string typeHeader = "Shape Type";
            const string colorHeader = "Color";
            const string perimeterHeader = "Perimeter";
            const string areaHeader = "Area";
            const string collisionHeader = "Collisions";

            string header = string.Format("{0, -20}{1, -20}{2, -20}{3, -20}{4, -20}\n", typeHeader, colorHeader,
                perimeterHeader, areaHeader, collisionHeader);

            this.listBox1.Items.Add(header);
        }

        private void resetTextBoxes()
        {
            this.numberShapesTextBox.ResetText();
            this.numberOfCircleTextBox.ResetText();
            this.numberOfRectangleTextBox.ResetText();
        }

        private void enableAllTextFields()
        {
            this.numberShapesTextBox.Enabled = true;
            this.numberOfCircleTextBox.Enabled = true;
            this.numberOfRectangleTextBox.Enabled = true;
        }

        private void disableAllButtons()
        {
            this.startButton.Enabled = false;
            this.ClearButton.Enabled = false;
            this.PauseButton.Enabled = false;
            this.ResumeButton.Enabled = false;
        }

        private void AreaRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.addItemsToListBox(this.canvasManager.GetAreaSortedCollection());
        }

        private void PerimeterRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.addItemsToListBox(this.canvasManager.GetPerimeterSortedCollection());
        }

        private void TypeColorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.addItemsToListBox(this.canvasManager.GetTypeColorSortedCollection());
        }

        private void CollisionTypeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.addItemsToListBox(this.canvasManager.GetCollisionTypeSortedCollection());
        }

        private void canvasPictureBox_Click(object sender, EventArgs e)
        {
            Point cursorCoords = Cursor.Position;
            cursorCoords = this.PointToClient(cursorCoords);
            const int leftBuffer = 14;
            const int topBuffer = 83;
            int x = cursorCoords.X - leftBuffer;
            int y = cursorCoords.Y - topBuffer;

            if (this.canvasManager.LocationContainsShape(x, y) && this.animationTimer.Enabled == false)
            {
                this.canvasManager.ChangeShapeColor();
            }
            this.Refresh();
            this.refreshCanvasAndList(sender, e);
        }

        private void refreshCanvasAndList(object sender, EventArgs e)
        {
            if (this.AreaRadioButton.Checked)
            {
                this.listBox1.Items.Clear();
                this.addHeader();
                this.AreaRadioButton_CheckedChanged(sender, e);
            }
            else if (this.PerimeterRadioButton.Checked)
            {
                this.listBox1.Items.Clear();
                this.addHeader();
                this.PerimeterRadioButton_CheckedChanged(sender, e);
            }
            else if (this.TypeColorRadioButton.Checked)
            {
                this.listBox1.Items.Clear();
                this.addHeader();
                this.TypeColorRadioButton_CheckedChanged(sender, e);
            }
            else if (this.CollisionTypeRadioButton.Checked)
            {
                this.listBox1.Items.Clear();
                this.addHeader();
                this.CollisionTypeRadioButton_CheckedChanged(sender, e);
            }
            else
            {
                this.listBox1.Items.Clear();
                this.addHeader();
                this.addItemsToListBox(this.canvasManager.Shapes);
            }
        }


        private void disabledButtonsOnAnimate()
        {
            this.startButton.Enabled = false;
            this.numberShapesTextBox.Enabled = false;
            this.numberOfCircleTextBox.Enabled = false;
            this.numberOfRectangleTextBox.Enabled = false;
        }

        private void enabledButtonsOnAnimate()
        {
            this.PauseButton.Enabled = true;
            this.ClearButton.Enabled = true;
        }

        #endregion
    }
}