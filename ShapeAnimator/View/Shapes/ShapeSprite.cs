﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public abstract class ShapeSprite
    {
        #region Instance variables

        private readonly Shape theShape;
        private SolidBrush theBrush;

        #endregion

        #region Properties          

        /// <summary>
        ///     Gets or sets the shape.
        /// </summary>
        /// <value>
        ///     The shape.
        /// </value>
        public Shape Shape
        {
            get { return this.theShape; }
        }

        /// <summary>
        ///     Gets the x.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        protected int X
        {
            get { return this.theShape.X; }
        }

        /// <summary>
        ///     Gets the y.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        protected int Y
        {
            get { return this.theShape.Y; }
        }

        /// <summary>
        ///     Gets or sets the width of the shape to be painted.
        /// </summary>
        /// <value>
        ///     The width of the shape.
        /// </value>
        protected int ShapeWidth
        {
            get { return this.theShape.Width; }
        }

        /// <summary>
        ///     Gets the height of the shape to be painted.
        /// </summary>
        /// <value>
        ///     The height of the shape.
        /// </value>
        protected int ShapeHeight
        {
            get { return this.theShape.Height; }
        }

        /// <summary>
        ///     Gets the brush.
        /// </summary>
        /// <value>
        ///     The brush.
        /// </value>
        protected SolidBrush TheBrush
        {
            get { return this.theBrush; }
            set { this.theBrush = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeSprite" /> class from being created.
        /// </summary>
        private ShapeSprite()
        {
            this.theShape = null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeSprite" /> class.
        ///     Precondition: shape != null
        /// </summary>
        /// <param name="shape">The shape.</param>
        protected ShapeSprite(Shape shape)
            : this()
        {
            if (shape == null)
            {
                throw new ArgumentNullException("shape");
            }

            this.theShape = shape;
            this.theBrush = new SolidBrush(this.theShape.Color);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public virtual void Paint(Graphics g)
        {
            this.TheBrush = new SolidBrush(this.Shape.Color);
        }

        /// <summary>
        ///     Gets the color of the sprite.
        /// </summary>
        /// <returns>A String representation of the brush color in (R,G,B)</returns>
        public String GetColorString()
        {
            string rValue = this.theBrush.Color.R.ToString("D3");
            string gValue = this.theBrush.Color.G.ToString("D3");
            string bValue = this.theBrush.Color.B.ToString("D3");

            String output = String.Format("({0,0}, {1,0}, {2,0})", rValue, gValue, bValue);
            return output;
        }

        #endregion
    }
}