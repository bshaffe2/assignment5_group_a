﻿using System;
using System.Drawing;

namespace ShapeAnimator.View.Shapes.Decorators
{
    /// <summary>
    /// Is the main decorator class that all the pattern decorators inherit from. 
    /// </summary>
    public abstract class SpriteDecoratorComponent : ShapeSprite
    {
        private readonly ShapeSprite baseSprite;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpriteDecoratorComponent" /> class.
        /// </summary>
        /// <param name="baseSprite">The base sprite.</param>
        protected SpriteDecoratorComponent(ShapeSprite baseSprite) : base(baseSprite.Shape)
        {
            if (baseSprite == null)
            {
                throw new Exception("Sprite cannot be null");
            }
            this.baseSprite = baseSprite;
        }

        /// <summary>
        ///     Draws a shape
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            this.baseSprite.Paint(g);
        }
    }
}