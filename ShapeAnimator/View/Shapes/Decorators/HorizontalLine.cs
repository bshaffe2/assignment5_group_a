﻿using System.Drawing;
using ShapeAnimator.View.Shapes.SpriteUtility;

namespace ShapeAnimator.View.Shapes.Decorators
{
     ///<summary>
    ///  is the a decorator class taht draws a horizontal line on a shape
    ///</summary>
    public class HorizontalLine : SpriteDecoratorComponent
    {
        private readonly Pen linePen;
        /// <summary>
        /// Initializes a new instance of the <see cref="HorizontalLine"/> class.
        /// </summary>
        /// <param name="theShapeSprite">The shape sprite.</param>
        public HorizontalLine(ShapeSprite theShapeSprite) : base(theShapeSprite)
        {
            this.linePen = new Pen(ColorFactory.GetARandomColor());
        }

        /// <summary>
        ///     Draws a shape
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);

            this.linePen.Width = 3;
            var startPoint = new Point();
            var endPoint = new Point();
            int verticalCenter = this.ShapeHeight/2;
            startPoint.X = this.X;
            startPoint.Y = this.Y + verticalCenter;
            endPoint.X = this.X + this.ShapeWidth;
            endPoint.Y = this.Y + verticalCenter;
            g.DrawLine(this.linePen, startPoint, endPoint);
        }
    }
}