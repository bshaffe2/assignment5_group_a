﻿using System.Drawing;
using ShapeAnimator.View.Shapes.SpriteUtility;

namespace ShapeAnimator.View.Shapes.Decorators
{
    /// <summary>
    /// is a decorator class that draws a spot on the shapes
    /// </summary>
    public class Spot : SpriteDecoratorComponent
    {
        private readonly Brush spotBrush;
        /// <summary>
        /// Initializes a new instance of the <see cref="Spot"/> class.
        /// </summary>
        /// <param name="theShapeSprite">The shape sprite.</param>
        public Spot(ShapeSprite theShapeSprite) : base(theShapeSprite)
        {
            this.spotBrush = new SolidBrush(ColorFactory.GetARandomColor());
        }

        /// <summary>
        ///     Draws a shape
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            var startPoint = new Point();
            int verticalCenter = this.ShapeHeight/2;
            int horizontalCenter = this.ShapeWidth/2;
            const int size = 12;
            const int alignmentBuffer = 6;
            startPoint.X = this.X + horizontalCenter;
            startPoint.Y = this.Y + verticalCenter;
            g.FillEllipse(this.spotBrush, startPoint.X - alignmentBuffer, startPoint.Y - alignmentBuffer, size, size);
        }
    }
}