﻿using System.Drawing;
using ShapeAnimator.View.Shapes.SpriteUtility;

namespace ShapeAnimator.View.Shapes.Decorators
{
    /// <summary>
    ///     Is a Decorator class that creates a vertical line in a shape
    /// </summary>
    public class VerticalLine : SpriteDecoratorComponent
    {
        private readonly Pen linePen;

        /// <summary>
        ///     Initializes a new instance of the <see cref="VerticalLine" /> class.
        /// </summary>
        /// <param name="theShapeSprite">The shape sprite.</param>
        public VerticalLine(ShapeSprite theShapeSprite) : base(theShapeSprite)
        {
            this.linePen = new Pen(ColorFactory.GetARandomColor());
        }

        /// <summary>
        ///     Draws a shape on the canvas
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);

            this.linePen.Width = 3;
            var startPoint = new Point();
            var endPoint = new Point();
            int horizontalCenter = this.ShapeWidth/2;
            startPoint.X = this.X + horizontalCenter;
            startPoint.Y = this.Y;
            endPoint.X = this.X + horizontalCenter;
            endPoint.Y = this.Y + this.ShapeHeight;
            g.DrawLine(this.linePen, startPoint, endPoint);
        }
    }
}