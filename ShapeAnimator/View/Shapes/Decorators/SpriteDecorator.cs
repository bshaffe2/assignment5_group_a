﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes.Decorators
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SpriteDecorator: ShapeSprite
    {
        private ShapeSprite theSprite;
        /// <summary>
        /// Initializes a new instance of the <see cref="SpriteDecorator"/> class.
        /// </summary>
        /// <param name="theShapeSprite">The shape sprite.</param>
        public SpriteDecorator(ShapeSprite theShapeSprite):base(theShapeSprite.Shape)
        {
            this.theSprite = theShapeSprite;
        }







    }
}
