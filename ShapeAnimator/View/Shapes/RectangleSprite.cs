﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Draws a Rectangle shape on the screen
    /// </summary>
    public class RectangleSprite : ShapeSprite
    {
        #region Constructors

        /// <summary>
        ///     Constructor for the RectangleSprite
        /// </summary>
        /// <param name="aShape"></param>
        public RectangleSprite(Shape aShape) : base(aShape)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        ///     precondition: g cannot be null
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            {
                if (g == null)
                {
                    throw new ArgumentNullException("g");
                }

                g.FillRectangle(this.TheBrush, this.X, this.Y, this.ShapeWidth, this.ShapeHeight);
            }
        }

        #endregion
    }
}