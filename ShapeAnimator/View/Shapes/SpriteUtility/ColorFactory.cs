﻿using System;
using System.Drawing;
using ShapeAnimator.Model;

namespace ShapeAnimator.View.Shapes.SpriteUtility
{
    /// <summary>
    ///     Is static a colorFactory class
    /// </summary>
    public static class ColorFactory
    {
        #region Methods

        /// <summary>
        ///     A color factory static method that returns a random Color
        /// </summary>
        /// <returns> Returns a random color </returns>
        public static Color GetARandomColor()
        {
            var names = (KnownColor[]) Enum.GetValues(typeof (KnownColor));
            Color randomColor = Color.Black;
            while (randomColor == Color.Black)
            {
                KnownColor randomColorName = names[Utility.RandomGeneratorNext(names.Length)];
                randomColor = Color.FromKnownColor(randomColorName);
            }

            return randomColor;
        }

        #endregion
    }
}