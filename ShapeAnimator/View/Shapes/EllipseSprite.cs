﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Draws a Circle shape on the screen
    /// </summary>
    public class EllipseSprite : ShapeSprite
    {
        #region Constructors

        /// <summary>
        ///     Constructor for the RectangleSprite
        /// </summary>
        /// <param name="aShape"></param>
        public EllipseSprite(Shape aShape)
            : base(aShape)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        ///     precondition: g cannot be null
        /// </summary>
        /// <param name="g">The graphics object to draw the shape one</param>
        /// <exception cref="System.ArgumentNullException">g</exception>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            {
                if (g == null)
                {
                    throw new ArgumentNullException("g");
                }

                g.FillEllipse(this.TheBrush, this.X, this.Y, this.ShapeWidth, this.ShapeHeight);
            }
        }

        #endregion
    }
}