﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeAnimator.Model;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimatorTester
{
    [TestClass]
    public class WhenShapesCollide
    {
        private readonly Ellipse aEllipse = new Ellipse(5,7);
        private readonly Ellipse bEllipse = new Ellipse(6,8);
        private readonly Rectangle aRectangle = new Rectangle(30, 40);
        private readonly Rectangle bRectangle = new Rectangle(120,4);
        private readonly List<Shape> testCollection = new List<Shape>();




        [TestMethod]
        public void WhenEmptyListShouldReturnFalse()
        {

            bool collide = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                        collide = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }
            Assert.AreEqual(false, collide);
        }


        [TestMethod]
        public void WhenOneItemInListShouldReturnFalse()
        {

            bool collide = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                        collide = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }
            Assert.AreEqual(false, collide);
        }

        [TestMethod]
        public void WhenTwoOverlappingShapesAIndexApartShouldCollide()
        {
          
            this.testCollection.Add(aEllipse);
            this.testCollection.Add(bEllipse);

            bool testCollision = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                      testCollision = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }

            bool expectedTrueCollision = aEllipse.BoundedRectangle.IntersectsWith(bEllipse.BoundedRectangle);
            Assert.AreEqual(expectedTrueCollision, testCollision);
        }


        [TestMethod]
        public void WhenTwoNonOverlappingShapesAIndexApartShouldNotCollide()
        {

            this.testCollection.Add(aEllipse);
            this.testCollection.Add(aRectangle);

            bool testCollision = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                        testCollision = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }
            bool expectedTrueCollision = aEllipse.BoundedRectangle.IntersectsWith(bEllipse.BoundedRectangle);
            Assert.AreEqual(expectedTrueCollision, testCollision);
        }


        [TestMethod]
        public void WhenOverlappingShapesMoreThanOneIndexApartShouldCollide()
        {

            this.testCollection.Add(aEllipse);
            this.testCollection.Add(aRectangle);
            this.testCollection.Add(bEllipse);

            bool testCollision = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                        testCollision = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }
            bool expectedTrueCollision = aEllipse.BoundedRectangle.IntersectsWith(bEllipse.BoundedRectangle);
            Assert.AreEqual(expectedTrueCollision, testCollision);
        }

        
        
        [TestMethod]
        public void WhenThreeNonOverLappingShapesShouldNotCollide()
        {

            this.testCollection.Add(aEllipse);
            this.testCollection.Add(aRectangle);
            this.testCollection.Add(bRectangle);

            bool testCollision = false;
            foreach (Shape aShape in this.testCollection)
            {
                foreach (Shape anotherShape in this.testCollection)
                {
                    if (aShape != anotherShape)
                    {
                        testCollision = (aShape.BoundedRectangle.IntersectsWith(anotherShape.BoundedRectangle));
                    }
                }
            }
            
            Assert.AreEqual(false, testCollision);
        }

       
    }
}